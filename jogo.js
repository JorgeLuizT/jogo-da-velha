var jogador = 0;
var jogadas = 0;

function clique(id) {
	console.log("jogador: "+jogador);
	
	var tag = null;
	
	if(id.currentTarget)
		tag = id.currentTarget; /* currentTarget é para obter o elemento que foi alvo do evento */
	else
		tag = document.getElementById('casa'+id);

	if(tag.style.backgroundImage == '' || tag.style.backgroundImage == null)
	{
		var endereco = 'img/'+jogador+'.jpg';
		tag.style.backgroundImage = 'url('+endereco+')';
		jogadas += 1;

		var ganhou = verificarGanhador(tag);
		


		switch(ganhou)
		{
			case 1:
				if(jogador == 0)
					alert("Homem aranha ganhou!!!");
				else
					alert("Duende verde ganhou!");
				break;
			case -1:
				alert("Empate!!!");
				break;
			case 0:
				// ninguem ganhou ainda
				break;
		}

		vez = document.getElementById('vez');
		if(jogador == 0)
		{
			jogador = 1;
			vez.innerHTML = 'Duende verde';
		}else
		{
			jogador = 0;
			vez.innerHTML = 'Homem aranha';
		}

		if(ganhou!=0)
		{
			finalizar();
			start = confirm('Deseja iniciar nova partida?');
			if(start)
				iniciar();
		}
	}
}


function verificarGanhador(jogada){
	const possibilidades = {
    'casa1': [[1, 2, 3], [1, 5, 9], [1, 4, 7]],
    'casa2': [[1, 2, 3], [2, 5, 8]],
    'casa3': [[1, 2, 3], [7, 5, 3], [3, 6, 9]],
    'casa4': [[4, 5, 6], [1, 4, 7]],
    'casa5': [[4, 5, 6], [1, 5, 9], [3, 5, 7], [2, 5, 8]],
    'casa6': [[4, 5, 6], [3, 6, 9]],
    'casa7': [[7, 8, 9], [3, 5, 7], [1, 4, 7]],
    'casa8': [[7, 8, 9], [2, 5, 8]],
    'casa9': [[7, 8, 9], [1, 5, 9], [3, 6, 9]],
  }; // Todas as possibilidades de vitória para cada casa
	let casa = jogada.id;
	var possibilidade = possibilidades[casa];

	for( let pos = 0; pos < possibilidade.length; pos++ ){
		let [casaA, casaB, casaC] = possibilidade[pos];
		let c1 = document.getElementById(`casa${casaA}`).style.backgroundImage;
		let c2 = document.getElementById(`casa${casaB}`).style.backgroundImage;
		let c3 = document.getElementById(`casa${casaC}`).style.backgroundImage;

		if( c1 == c2 && c1 == c3 && c1 != '' ) {
			return 1; // Para 1, se tem o vencedor.
		};
	};

	if( jogadas == 9) {
		return -1;
	};

	return 0;
}



function finalizar(){
	tags = document.getElementsByClassName('casa');

	for(i=0; i<9; i++){
		tags[i].onclick = null;
	}
}

function iniciar() {
	jogador = 0;
	jogadas = 0;

	casas = document.getElementsByClassName('casa');

	for(i=0; i<9; i++){
		casas[i].onclick=clique;
		casas[i].style.backgroundImage='';
	}

	vez = document.getElementById('vez');
	vez.innerHTML = 'Homem aranha';

}

// 1- verificar ganhador OK
// 2- exibir resultado OK
// 3- Mensagem quando der velha. OK
// 4- Indicar o jogador da vez - OK
// 5- Iniciar nova partida - OK
// 6- Finalização do jogo - OK
// 7- Placar (usando localStorage) https://www.w3schools.com/jsref/prop_win_localstorage.asp

/**
Itens do Trabalho (individual)
1- Melhorar quando se começa a verificar ganhador (a partir da 5 jogada)
2- Melhorar a verificação das linhas para saber se alguém ganhou...
 reduzir de 8 verificações para o mínimo.
4- Trocar alert por mensagem de texto no html.
5- Definir personagens  antagonistas e visual harmonico para o tabuleiro (css)
*/
